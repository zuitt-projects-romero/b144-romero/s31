// Controllers contain the functionas and business logic of our express js app
// meaning all the operations can do will be placed in this file
const Task = require('../models/task');

// Controller function for getting all the tasks
module.exports.getAllTasks=()=>{
	return Task.find({}).then(result=>{
		return result;
	})//from task routes, returns from task routes
}

//Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task ({
		name: requestBody.name
	})

	// SAVE
	// The "then" method will accept the 2 parametes 
		//  the first parameter will store the result returned by the mongoose "save" method
		// The second parameter will store the "error" object
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			// If an error is encountered, the 'return' statement will prevent any other line or code below and within the same code block from executing.
			// Since the following return statement is nested within the 'then' method chained to the "save" method, they do not prevent each other from executing code
			// the else statment will no longer be evaluated 
			return false;
		} else{
			// if save is successful return the new task Object

			return task;
		}

	})
}


// Controller function for deleting a taskk

/*	Business Logic in deleting a certain document
1.Look or find for the task with the corresponding id provided in the url/route
2. Delete the task
*/

// Creating the function

module.exports.deleteTask = (taskId) => {
	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB it looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		}else {
			return removedTask;
		}
	})
}


// Updating a task

/*
	Business Logic
1. Get the task with the ID
2. Replace the task's name returned from the database wit the "name" property from the request body
3. Save the task
*/


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		// if an error is encountered return a false
		if(error){
			console.log(error );
			return false;
		}
		// Results of the "findById" method will be stored in the "result" parameter
		// It's "name" property will be assigned the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return false
			}else{
				return updatedTask
			}
		})
	})
}


// Activiy updating a task status to complete


// No. 2


module.exports.getSpecificTasks=(taskId)=>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false
		}
		else{
		return result
		}
	})
}

// No. 6
// module.exports.updateStatus = (taskId, newContent) => {
// 	return Task.findById(taskId).then((result, error) => {
		
// 		if (error) {
// 			console.log(error);
// 			return false;
// 		}
// 		result.status = newContent.status;

// 		return result.save().then((updatedTaskStatus, error) => {
// 			if (error) {
// 				console.log(error)
// 				return false;
// 			} 
// 			else {
// 				return updatedTaskStatus;
// 			}
// 		})
// 	})
// }

module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}

		result.status = "complete"
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return saveErr
			}else{
				return updatedTask
			}
		})
	})
}



/*
	Business Logic
1. Get the task with the ID
2. Change the Status from pending to complete
3. Save the task
*/

/*
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		// if an error is encountered return a false
		if(error){
			console.log(error );
			return false;
		}
		// Results of the "findById" method will be stored in the "result" parameter
		// It's "name" property will be assigned the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return false
			}else{
				return updatedTask
			}
		})
	})
}*/


