//index.js
// Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use all the routes defined in 'taskRoute/js'
const taskRoute = require('./routes/taskRoute');

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}))

// Database connection
mongoose.connect("mongodb+srv://brands27:nevermore27@cluster0.eteec.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	// always put this when connecting in mongoDB
	useNewUrlparser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open",() => console.log("We're connected to the cloud database"))

// Routes(Base URI for task route)
// Allows all the task routes created in the taskRoute.js file to use "/tasks"

app.use("/tasks", taskRoute)
//http://localhost:3001/tasks

app.listen(port,()=>console.log(`Now listens to port ${port}`))