const express = require('express');
const taskController = require('../controllers/taskController');

// Create a Router instance that functions as a middleware and routing system allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// Route to get all the tasks
// http:/localhost:3001/tasks/
router.get("/",(req,res) =>{
	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController));
})

//Create a route for creating a task
router.post("/", (req,res) =>{
	taskController.createTask(req.body).then(result => res.send(result));
}) 

// Route for deleting a task

// http://locallhost:3001/task/:id
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply in the url
// Ther wor that comes after the colon symbol will be the name of the URL parameter
// ":id" is called WILDCARD were you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL

router.delete("/:id", (req, res) => {
	// The URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter namee 
	// In this case "Id" is the name of the paraeter
	// 
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

//Update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})

/*Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of s31 Activity.
11. Add the link in Boodle named Express Js Modules, Parameterized Routes.*/

// No. 1
router.get("/:id",(req,res) =>{
	taskController.getSpecificTasks(req.params.id).then(resultFromController=>res.send(resultFromController));
})


// No.5 
router.patch("/:id/complete", (req,res) =>{
	taskController.updateStatus(req.params.id).then(result => res.send(result));
}) 

module.exports = router;